import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:developer';
import 'package:intl/intl.dart';


Future<List<Game>> fetchActualGames() async {
  //return http.get('https://www.openligadb.de/api/getmatchdata/bl1');
  final response =
      await http.get('https://www.openligadb.de/api/getmatchdata/bl1');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    log('data: $response.body');
    return parseGames(response.body);
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

// A function that will convert a response body into a List<Photo>
List<Game> parseGames(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Game>((json) => Game.fromJson(json)).toList();
}

class Team {
  final int id;
  final String title;
  final String logo;

  Team({this.id, this.title, this.logo});

  factory Team.fromJson(Map<String, dynamic> json) {
    return Team(
      id: json['TeamID'],
      title: json['TeamName'],
      logo : json['TeamIconUrl']
    );
  }
}

class Game {
  final int id;
  final Team team1;
  final Team team2;
  final String matchDateTime;

  Game({this.id, this.team1, this.team2, this.matchDateTime});

  factory Game.fromJson(Map<String, dynamic> json) {
    return Game(
      id: json['MatchID'],

      team1: Team.fromJson(json['Team1']),
      team2: Team.fromJson(json['Team2']),
      matchDateTime:  json['MatchDateTime'] 
    );
  }
}

void main() => runApp(MyApp(games: fetchActualGames()));

class MyApp extends StatelessWidget {
  final Future<List<Game>> games;

  MyApp({Key key, this.games}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: FutureBuilder<List<Game>>(
            future: games,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                log('data: $games');
                //return Text("has data") ;
                return createListView(context, snapshot);
                //return Text((snapshot.data.team1 as Team).title);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}

Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
  List<Game> games = snapshot.data;
  return new ListView.builder(
    itemCount: games.length,
    itemBuilder: (BuildContext context, int index) {
      var game = games[index]  ;
      var team1 = game.team1;
      var team2 = game.team2;
      var matchdate = DateTime.parse(game.matchDateTime)  ;
      var mdateFormatted = DateFormat("dd.MM.yyyy HH:mm").format(matchdate)   ;
      return new Column(
        children: <Widget>[
          new Row(children: <Widget>[
            Expanded(
              child: Text(mdateFormatted, textAlign: TextAlign.center),
            ),
            Expanded(
              child: Image.network(team1.logo, height: 30)
            ),
            Expanded(
              child: Text(team1.title, textAlign: TextAlign.center),
            ),
            Expanded(
              child: Text(team2.title, textAlign: TextAlign.center),
            ),
            Expanded(
                child: Image.network(team2.logo, height : 30)
            ),
          ]),
          new Divider(
            height: 2.0,
          ),
        ],
      );
    },
  );
}
